<?php

class Base_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function get_all_data($table)
  {
    return $this->db->get($table);
  }

  public function get_data_by($table, $field, $data)
  {
    return $this->db->get_where($table, [$field => $data]);
  }

  public function insert_data($table, $data)
  {
    $this->db->insert($table, $data);
  }

  public function delete_data($table, $field, $data)
  {
    $this->db->delete($table, [$field => $data]);
  }

  public function update_data_text($table, $data)
  {
    $this->db->update($table, $data);
  }

  public function update_data_by($table, $data, $field, $key)
  {
    $this->db->update($table, $data, [$field => $key]);
  }

  public function get_data_count($table)
  {
    return $this->db->count_all_results($table);
  }

  public function get_last_id()
  {
    $this->db->select(['id']);
    $this->db->from('data');
    $this->db->order_by('id', DESC);
    $this->db->limit(1);
    return $this->db->get();
  }

  public function get_data_and_file($id)
  {
    $sql = "Select data.id as data_id, file.id as file_id, file.name as filename
            from data join file on data.id = file.data_id
            WHERE 1=1 AND data.id = ?";
    $query = $this->db->query($sql, $id);
    return $query;
  }
}
