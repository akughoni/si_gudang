<div class="card p-2 text-center">
  <div class="col-12 mb-1 d-flex flex-row align-items-center justify-content-between">
    <h4 class="m-0 font-weight-bold text-gray">File Manager</h4>
    <a class="btn btn-primary" href="<?= base_url(); ?>dashboard/add_file?data_id=<?= $data_id ?>">Add File</a>
  </div>
</div>
<div class="row mt-2">
  <?php foreach ($list_item as $key => $item) : ?>
    <div class="col-4">
      <div class="card shadow mb-4">
        <div class="card-body ">
          <div class="card-img">
            <div class="row">
              <div class="col-12 mb-1 d-flex flex-row align-items-center justify-content-between">
                <p class="m-0 font-weight-bold text-gray"><?= $item->name ?></p>
                <div class="dropdown no-arrow">
                  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-header">Actions :</div>
                    <a class="dropdown-item" href="<?= base_url(); ?>data/delete_file?id=<?= $item->id ?>&data_id=<?= $data_id ?>">Delete Item</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <img class="card-img col" style="width: 25rem;" src="<?= $item->location . $item->name; ?>" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach ?>
</div>