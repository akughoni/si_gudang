<div class="card shadow">
  <div class="card-header">
    <h4 class="m-0 font-weight-bold text-primary text-center">Insert Data</h4>
  </div>
  <div class="card-body">
    <form action="<?= base_url("data/store_input_data") ?>" id="dataForm" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label for="buku">Buku Tanah</label>
        <input name="buku" value="" type="text" class="form-control" id="buku" placeholder="">
        <small class="form-text text-danger"><?= form_error('') ?></small>
      </div>
      <div class="row">
        <div class="form-group col-6">
          <label for="kecamatan">Kecamatan</label>
          <select class="form-control" name="kecamatan" id="">
            <option selected disabled>Select..</option>
            <option value="1">Kecamatan 1</option>
            <option value="2">Kecamatan 2</option>
            <option value="3">Kecamatan 3</option>
          </select>
          <small class="form-text text-danger"><?= form_error('') ?></small>
        </div>
        <div class="form-group col-6">
          <label for="kelurahan">Kelurahan</label>
          <select class="form-control" name="kelurahan" id="">
            <option selected disabled>Select..</option>
            <option value="1">Kelurahan 1</option>
            <option value="2">Kelurahan 2</option>
            <option value="3">Kelurahan 3</option>
          </select>
          <small class="form-text text-danger"><?= form_error('') ?></small>
        </div>
      </div>
      <div class="row">
        <div class="form-group col-6">
          <label for="alamat">Alamat</label>
          <textarea name="alamat" class="form-control" rows="5" id="alamat"></textarea>
          <small class="form-text text-danger"><?= form_error('') ?></small>
        </div>
        <div class="col-6">
          <div class="form-group ">
            <label for="hmilik">M (Hak Milik)</label>
            <input name="hmilik" value="" type="text" class="form-control" id="hmilik" placeholder="">
            <small class="form-text text-danger"><?= form_error('') ?></small>
          </div>
          <div class="form-group ">
            <label for="nomer">Nomor</label>
            <input name="nomer" value="" type="text" class="form-control" id="nomer" placeholder="">
            <small class="form-text text-danger"><?= form_error('') ?></small>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="form-group col-6">
          <label for="bangunan">B (Bangunan)</label>
          <input name="bangunan" value="" type="text" class="form-control" id="bangunan" placeholder="">
          <small class="form-text text-danger"><?= form_error('') ?></small>
        </div>
        <div class="form-group col-6">
          <label for="hpakai">HP (Hak Pakai)</label>
          <input name="hpakai" value="" type="text" class="form-control" id="hpakai" placeholder="">
          <small class="form-text text-danger"><?= form_error('') ?></small>
        </div>
      </div>
      <div class="form-group">
        <label for="hrms">HRMS</label>
        <input name="hrms" value="" type="text" class="form-control" id="hrms" placeholder="">
        <small class="form-text text-danger"><?= form_error('') ?></small>
      </div>
      <div class="form-group">
      </div>
    </form>
    <button form="dataForm" class="mt-2 btn btn-primary float-right" type="submit">Submit</button>
  </div>
</div>