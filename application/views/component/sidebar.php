<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url() ?>">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">SIGudang</div>
  </a>
  <!-- Divider -->
  <hr class="sidebar-divider">
  <li class="nav-item">
    <!-- <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseHome" aria-expanded="true" aria-controls="collapseHome">
      Tampil Data
    </a>
    <div id="collapseHome" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="<?= base_url() ?>dashboard/">Tampil Data</a>
        <a class="collapse-item" href="<?= base_url() ?>dashboard/input_data">Input Data</a>
      </div>
    </div> -->
    <a class="nav-link" href="<?= base_url() ?>dashboard/">Data</a>
    <a class="nav-link" href="<?= base_url() ?>dashboard/input_data">Insert</a>
  </li>
  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>