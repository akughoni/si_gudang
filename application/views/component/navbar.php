<!-- <div id="content-wrapper" class="d-flex flex-column"> -->
<!-- Main Content -->
<!-- <div id="content"> -->

<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
  <div class="container">
    <!-- Topbar Navbar -->
    <div class="navbar-brand">
      <a href="<?= base_url() ?>dashboard/" class="text-primary">
        <h2 class="font-weight-bold">SIGudang</h2>
      </a>
    </div>
    <ul class="navbar-nav ml-auto">
      <?php if ($this->ion_auth->is_admin()) : ?>
        <li class="nav-item">
          <a href="<?= base_url() ?>dashboard/input_data" class="btn btn-primary mr-2 ">Insert Data</a>
        </li>
        <li class="nav-item">
          <a href="<?= base_url() ?>dashboard" class="btn btn-primary">Show Data</a>
        </li>
      <?php endif; ?>
      <div class="topbar-divider d-none d-sm-block"></div>
      <!-- Nav Item - User Information -->
      <li class="nav-item">
        <a class="btn btn-danger" href="<?= base_url() ?>auth/logout">Logout</a>
      </li>
    </ul>
  </div>
</nav>
<!-- </div>
</div> -->