<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Login Page</title>
  <link href="<?= base_url() ?>dist/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="<?= base_url() ?>dist/css/sb-admin-2.min.css" rel="stylesheet">
</head>
<!-- <body> -->
<!-- mulai dari body -->
<?php $this->load->view($content_page); ?>

<script src="<?= base_url() ?>dist/vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>dist/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>dist/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="<?= base_url() ?>dist/js/sb-admin-2.min.js"></script>>
</body>

</html>