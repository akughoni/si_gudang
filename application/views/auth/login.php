<body class="bg-gradient-primary">
	<div class="container">
		<div class="row justify-content-center align-middle">
			<div class="col-5 my-5">
				<?php if ($this->session->flashdata('message')) { ?>
					<div class="alert <?= $this->session->flashdata('alert') ?>">
						<span><?= $this->session->flashdata('message') ?></span>
					</div>
				<?php } ?>
				<div class="card o-hidden border-0 shadow-lg">
					<div class="card-body p-0">
						<!-- Nested Row within Card Body -->
						<div class="row">
							<div class="col">
								<div class="p-5">
									<div class="text-center">
										<h1 class="h4 text-gray-900 mb-4">Login Page</h1>
									</div>
									<hr>
									<form class="user" method="post" action="<?= base_url() ?>auth/login">
										<div class="form-group">
											<input type="text" name="identity" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Enter email...">
											<small class="text text-danger text-center form-text"><?= form_error('identity'); ?></small>
										</div>
										<div class="form-group">
											<input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
											<small class="text text-danger text-center form-text"><?= form_error('password'); ?></small>
										</div>
										<div class="form-group">
											<div class="custom-control custom-checkbox small">
												<input type="checkbox" class="custom-control-input" id="remember" name="remember">
												<label class="custom-control-label" for="remember">Remember Me</label>
											</div>
										</div>
										<button type="submit" class="btn btn-primary btn-user btn-block">
											Login
										</button>
										<hr>
									</form>
									<div class="text-center">
										<a class="small" href="forgot-password.html">Forgot Password?</a>
									</div>
									<div class="text-center">
										<a class="small" href="<?= base_url() ?>auth/register">Create an Account!</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- </body> -->