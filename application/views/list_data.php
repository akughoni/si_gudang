<div class="card shadow">
  <div class="card-header">
    <h4 class="font-weight-bold text-primary text-center">Data Gudang</h4>
  </div>
  <div class="card-body">
    <table id="example" class="table table-striped table-bordered" style="width:100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Buku Tanah</th>
          <th>Kecamatan</th>
          <th>Kelurahan</th>
          <th>Alamat</th>
          <th>Hak Milik</th>
          <th>Nomor</th>
          <th>Bangunan</th>
          <th>Hak Pakai</th>
          <th>HRMS</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($data as $value) : ?>
          <tr>
            <td><?= $value->id ?></td>
            <td><?= $value->buku_tanah ?></td>
            <td><?= $value->kecamatan ?></td>
            <td><?= $value->kelurahan ?></td>
            <td><?= $value->alamat ?></td>
            <td><?= $value->hak_milik ?></td>
            <td><?= $value->nomor ?></td>
            <td><?= $value->bangunan ?></td>
            <td><?= $value->hak_pakai ?></td>
            <td><?= $value->hrms ?></td>
            <td>
              <a class="badge badge-primary" href="<?= base_url() . "dashboard/list_file?data_id=" . $value->id ?>">Lihat File</a>
              <a class="badge badge-warning" href="<?= base_url() . "dashboard/update_data?data_id=" . $value->id; ?>">Update</a>
              <a class="badge badge-danger" href="<?= base_url() . "data/delete_data?data_id=" . $value->id ?>">Delete</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>