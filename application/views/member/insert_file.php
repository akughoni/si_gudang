<div class="card shadow">
  <div class="card-header">
    <h4 class="m-0 font-weight-bold text-primary text-center">Insert File</h4>
  </div>
  <div class="card-body">
    <dir class="align-center">
      <form action="<?= base_url() ?>data/store_input_file" class="dropzone"></form>
      <div class="text-center">
        <a href="<?= base_url(); ?>" class="mt-2 btn btn-primary">Selesai</a>
      </div>
    </dir>
  </div>
</div>