<?php
require_once 'Base.php';

class Data extends Base
{
  public function __construct()
  {
    parent::__construct();
    if ($this->login_check() == false) {
      redirect('auth/');
    } elseif (!$this->ion_auth->is_admin()) {
      redirect('member/');
    }
  }

  public function store_input_data()
  {
    $this->form_validation->set_rules('buku', 'buku', 'required');
    $this->form_validation->set_rules('kecamatan', 'kecamatan', 'required');
    $this->form_validation->set_rules('kelurahan', 'kelurahan', 'required');
    $this->form_validation->set_rules('alamat', 'alamat', 'required');
    $this->form_validation->set_rules('hmilik', 'hmilik', 'required');
    $this->form_validation->set_rules('nomer', 'nomer', 'required');
    $this->form_validation->set_rules('bangunan', 'bangunan', 'required');
    $this->form_validation->set_rules('hpakai', 'hpakai', 'required');
    $this->form_validation->set_rules('hrms', 'hrms', 'required');

    if ($this->form_validation->run()) {
      $insertData['buku_tanah'] = $this->input->post('buku');
      $insertData['kecamatan'] = $this->input->post('kecamatan');
      $insertData['kelurahan'] = $this->input->post('kelurahan');
      $insertData['alamat'] = $this->input->post('alamat');
      $insertData['hak_milik'] = $this->input->post('hmilik');
      $insertData['nomor'] = $this->input->post('nomer');
      $insertData['bangunan'] = $this->input->post('bangunan');
      $insertData['hak_pakai'] = $this->input->post('hpakai');
      $insertData['hrms'] = $this->input->post('hrms');

      $this->base_model->insert_data('data', $insertData);
      if ($this->ion_auth->is_admin()) {
        redirect('dashboard/');
      } else {
        redirect('member/file');
      }
    } else {
      $data['content_page'] = 'insert_data';
      $this->template($data);
    }
  }

  public function store_update_data()
  {
    $this->form_validation->set_rules('buku', 'buku', 'required');
    $this->form_validation->set_rules('kecamatan', 'kecamatan', 'required');
    $this->form_validation->set_rules('kelurahan', 'kelurahan', 'required');
    $this->form_validation->set_rules('alamat', 'alamat', 'required');
    $this->form_validation->set_rules('hmilik', 'hmilik', 'required');
    $this->form_validation->set_rules('nomer', 'nomer', 'required');
    $this->form_validation->set_rules('bangunan', 'bangunan', 'required');
    $this->form_validation->set_rules('hpakai', 'hpakai', 'required');
    $this->form_validation->set_rules('hrms', 'hrms', 'required');

    if ($this->form_validation->run()) {
      $id = $this->input->post('id');
      $dataUpdate['buku_tanah'] = $this->input->post('buku');
      $dataUpdate['kecamatan'] = $this->input->post('kecamatan');
      $dataUpdate['kelurahan'] = $this->input->post('kelurahan');
      $dataUpdate['alamat'] = $this->input->post('alamat');
      $dataUpdate['hak_milik'] = $this->input->post('hmilik');
      $dataUpdate['nomor'] = $this->input->post('nomer');
      $dataUpdate['bangunan'] = $this->input->post('bangunan');
      $dataUpdate['hak_pakai'] = $this->input->post('hpakai');
      $dataUpdate['hrms'] = $this->input->post('hrms');

      $this->base_model->update_data_by('data', $dataUpdate, 'id', $id);
      redirect('dashboard/');
    } else {
      redirect('dashboard/');
    }
  }

  public function delete_data()
  {
    $id = $this->input->get("data_id");
    $query = $this->base_model->get_data_and_file($id)->result();
    foreach ($query as $value) {
      $del_status = unlink('./upload_foto/' . $value->filename);
      $this->base_model->delete_data('file', 'id', $value->file_id);
    }
    if ($del_status) {
      $this->base_model->delete_data('data', 'id', $id);
      redirect('dashboard/');
    } else {
      redirect('dashboard/');
    }
  }

  public function store_input_file()
  {
    $config['upload_path']   =  './upload_foto/';
    $config['allowed_types'] = 'gif|jpg|png|ico';
    $this->load->library('upload', $config);

    if ($this->upload->do_upload('file')) {
      $result = $this->base_model->get_last_id()->row();
      $data['name'] = $this->upload->data('file_name');
      $data['location'] = base_url() . "upload_foto/";
      $data['data_id'] = $result->id;
      $this->base_model->insert_data('file', $data);
      if ($this->ion_auth->is_admin()) {
        redirect('dashboard/input_file');
      } else {
        redirect('member/');
      }
    }
  }

  public function store_add_file()
  {
    $config['upload_path']   =  './upload_foto/';
    $config['allowed_types'] = 'gif|jpg|png|ico';
    $id = $this->input->get("data_id");
    $this->load->library('upload', $config);

    if ($this->upload->do_upload('file')) {
      // $result = $this->base_model->get_last_id()->row();
      $data['name'] = $this->upload->data('file_name');
      $data['location'] = base_url() . "upload_foto/";
      $data['data_id'] = $id;
      $this->base_model->insert_data('file', $data);
      redirect('dashboard/input_file');
    }
  }

  public function delete_file()
  {
    $id = $this->input->get('id');
    $data_id = $this->input->get('data_id');
    $query = $this->base_model->get_data_by('file', 'id', $id)->row();
    $name_file = $query->name;
    $del_status = unlink('./upload_foto/' . $name_file);
    if ($del_status) {
      $this->base_model->delete_data('file', 'id', $id);
      redirect('dashboard/list_file?data_id=' . $data_id);
    } else {
      redirect('dashboard/list_file?data_id=' . $data_id);
    }
  }
}
