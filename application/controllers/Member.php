<?php

require "Base.php";

class Member extends Base
{
  public function __construct()
  {
    parent::__construct();
    if ($this->login_check() == false) {
      redirect('auth/');
    }
  }

  public function index()
  {
    $data['content_page'] = 'member/insert_data';
    $this->Member_template($data);
  }

  public function file()
  {
    $data['content_page'] = 'member/insert_file';
    $this->Member_template($data);
  }
}
