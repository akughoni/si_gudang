<?php

class Base extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('base_model');
    $this->load->library('form_validation');
    $this->load->library('ion_auth');
  }
  public function login_check()
  {
    $test = $this->ion_auth->logged_in();
    return $test;
  }

  public function template($data = null)
  {
    $this->load->view('template', $data);
  }

  public function Auth_template($data = null)
  {
    $this->load->view('auth/template', $data);
  }
  public function Member_template($data = null)
  {
    $this->load->view('member/template', $data);
  }
  public function check_username($data = null)
  {
    $query = $this->auth_model->user_check($data)->row();
    if (isset($query)) {
      return true;
    } else {
      return false;
    }
  }
}
