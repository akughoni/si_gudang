<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'Base.php';

class Dashboard extends Base
{
  public function __construct()
  {
    parent::__construct();
    if ($this->login_check() == false) {
      redirect('auth/');
    } elseif (!$this->ion_auth->is_admin()) {
      redirect('member/');
    }
  }
  public function index()
  {
    $result = $this->base_model->get_all_data('data')->result();
    $data['data'] = $result;
    $data['content_page'] = 'list_data';
    $this->template($data);
  }

  public function input_data()
  {
    $data['content_page'] = 'insert_data';
    $this->template($data);
  }
  public function input_file()
  {
    $data['content_page'] = 'insert_file';
    $this->template($data);
  }

  public function add_file()
  {
    $data['data_id'] = $this->input->get("data_id");
    $data['content_page'] = 'add_file';
    $this->template($data);
  }

  public function update_data()
  {
    $id = $this->input->get('data_id');
    $result = $this->base_model->get_data_by('data', 'id', $id)->row();
    $data['content_page'] = 'update_data';
    $data['data'] = $result;
    $this->template($data);
  }

  public function list_file()
  {
    $id = $this->input->get('data_id');
    $result = $this->base_model->get_data_by('file', 'data_id', $id);
    $data['data_id'] = $id;
    $data['list_item'] = $result->result();
    $data['content_page'] = 'list_file';
    $this->template($data);
  }
}
