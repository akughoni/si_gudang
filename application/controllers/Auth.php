<?php
require 'Base.php';

class Auth extends Base
{
  public function __construct()
  {
    parent::__construct();
    $this->load->helper(['url', 'language']);
  }
  public function index()
  {
    $data['content_page'] = 'auth/login';
    $this->Auth_template($data);
  }

  public function login()
  {
    // validate form input
    $this->form_validation->set_rules('identity', 'Email', 'required');
    $this->form_validation->set_rules('password', 'password', 'required');

    if ($this->form_validation->run() === TRUE) {
      $remember = (bool) $this->input->post('remember');

      if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
        redirect('/', 'refresh');
      } else {
        redirect('auth', 'refresh');
      }
    } else {

      redirect('auth', 'refresh');
    }
  }
  public function logout()
  {
    $this->ion_auth->logout();
    redirect('auth/login', 'refresh');
  }

  public function register()
  {
    $data['content_page'] = "auth/register";
    $this->Auth_template($data);
  }

  public function create_user()
  {
    $tables = $this->config->item('tables', 'ion_auth');
    $identity_column = $this->config->item('identity', 'ion_auth');
    $this->data['identity_column'] = $identity_column;

    // validate form input
    if ($identity_column !== 'email') {
      $this->form_validation->set_rules('identity', $this->lang->line('create_user_validation_identity_label'), 'trim|required|is_unique[' . $tables['users'] . '.' . $identity_column . ']');
      $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email');
    } else {
      $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]');
    }
    $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[password_confirm]');
    $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

    if ($this->form_validation->run() === TRUE) {
      $email = strtolower($this->input->post('email'));
      $identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
      $password = $this->input->post('password');
    }
    if ($this->form_validation->run() === TRUE && $this->ion_auth->register($identity, $password, $email)) {
      // check to see if we are creating the user
      // redirect them back to the admin page
      // die('sini');
      redirect("/");
    } else {
      //
    }
  }
}
